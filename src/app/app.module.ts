import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


// Animations  Step 1  for Animations We Need To Add This .

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

 // step 3

import {MatButtonModule, 
  MatCheckboxModule ,
    MatCardModule,
   MatInputModule,
   MatToolbarModule,
   MatSnackBarModule} from '@angular/material';


import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';

import { WebService } from './service/web.service';

import { HttpModule } from '@angular/http';
import { NewMessageComponent } from './new-message/new-message.component';

import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';


import {RegisterComponent} from './register.component'
import { AuthService } from './auth.service';
// step 2 BrowserAnimationsModule we need to Add in imports List
 
// for step 4 Go to style.css 

const routes: Routes = [
  { path: 'register', component: RegisterComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    RegisterComponent,
    NewMessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    MatSnackBarModule,
    RouterModule.forRoot(routes)
  ],
  providers: [WebService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
