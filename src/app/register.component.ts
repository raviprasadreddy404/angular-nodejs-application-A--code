import { Component } from '@angular/core';


import {FormBuilder,Validators} from '@angular/forms' 

import { AuthService } from './auth.service';
@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styles : [`.error{background-color : yellow}`]
  
 // styleUrls: ['.register.component.css']
})
export class RegisterComponent {

    form;
    constructor(private fb : FormBuilder, private authservice : AuthService){
//in FormGroup 1st paramameter is model, 2nd Parameter is Validator

        this.form = fb.group({

            firstName: ['',Validators.required],
            lastName :'',
            email : ['',[Validators.required,this.emailValidator()]],
            password :'',
            confirmpassword : ''            
        },{ validator : this.matchingFields('password','confirmpassword')})
    }

    onSubmit(){

       console.log(this.form.value);
       console.log(this.form.errors);
        this.authservice.register(this.form.value)
    }

    // confirm password and password validation

   matchingFields(field1,field2) {
            return form =>{
                if(form.controls[field1].value! === form.controls[field2].value)
                    return  {mismatchedFields :true}
                

            }
    }

    // in this control is form control object

    emailValidator(){

        return control =>{
            var regex  = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            
            return regex.test(control.value)? null :{ invalidMail : true}
        }
    }
} 
