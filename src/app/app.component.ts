import { Component,ViewChild } from '@angular/core';
import { MessagesComponent } from './messages/messages.component';
import { NewMessageComponent } from './new-message/new-message.component';
import { AuthService } from './auth.service';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor( public auth : AuthService){}

@ViewChild(MessagesComponent)  msg : MessagesComponent;

//@ViewChild(NewMessageComponent)  msg : NewMessageComponent;





 
  onPosted(messages){

   // console.log(messages);
     this.msg.messages.push(messages);

}

}
