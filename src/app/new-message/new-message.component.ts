import { Component, OnInit ,Output , EventEmitter } from '@angular/core';
import { WebService } from '../service/web.service';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent  {

@Output() onPosted = new EventEmitter();

  constructor( public webservice :WebService ) { }

 
  messages ={
    name : '',
    text :''
  }

 // name = 'asdfg';

   Post(){
    this.webservice.postMessages(this.messages);

    this.onPosted.emit(this.messages); 

//    console.log(this.messages);
  }

}
