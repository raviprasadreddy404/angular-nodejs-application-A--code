import { Component, OnInit } from '@angular/core';

import {WebService} from '../service/web.service'
 
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent  {


  

  constructor(public webservice : WebService) { }

// (async and await) are es6 Properties so we need to change in  tsconfig.json 

  async ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
    var response = await this.webservice.getMessages();

    this.messages = response.json();
  
  }

  messages = [];



//  messages = [{name:'ravi ', text :"Iam  Ravi "},{name:'Prasad ', text :"Iam  Prasad "}];


}
