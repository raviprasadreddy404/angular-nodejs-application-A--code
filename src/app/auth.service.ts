import { Injectable } from "@angular/core";





import { Http } from "@angular/http";
import { Router } from "@angular/router";

@Injectable()

export class AuthService{

     base_URL = 'http://localhost:7007/auth';
    Name_Key = 'name';
    TOKEN_KEY = 'token';

    constructor(public http :Http, public router : Router){}



    get name(){
        return localStorage.getItem(this.Name_Key)
    }

    // to get true or false we are using !!
    get isAuthenticated(){
        return !!localStorage.getItem(this.TOKEN_KEY)
    }


    register(user){
        // to deslete the confirmpassword at backend side we use delete 

        // to store the token we use 
        // we have 2 params in localstorage 1st 'key' ie 'token' 2nd params token value 
        delete user.confirmpassword;
        this.http.post(this.base_URL + '/register', user)
        .subscribe(res =>{

                var authresponse = res.json();

                if(!authresponse.token)
                return ;

            localStorage.setItem(this.TOKEN_KEY,res.json().token) 
            localStorage.setItem(this.Name_Key,res.json().FirstName) 
            this.router.navigate(['/']);
    });
    }

    logout(){

        localStorage.removeItem(this.Name_Key);
        localStorage.removeItem(this.TOKEN_KEY);
        
    }
}