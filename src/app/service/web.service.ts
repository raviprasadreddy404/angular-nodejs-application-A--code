import {Http} from '@angular/http'

import 'rxjs/add/operator/toPromise';

 
import { Injectable } from '@angular/core';


@Injectable()
export class WebService {


    constructor( public http: Http){

    }
    getMessages(){
       return  this.http.get('http://localhost:7007/user/messages').toPromise();


    }

    postMessages(messages){
        return  this.http.post('http://localhost:7007/user/messages', messages).toPromise();
    }
}